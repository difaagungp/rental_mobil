<?php

namespace App\Http\Controllers;

use App\Models\Armada;
use App\Models\Mobil;
use Illuminate\Http\Request;

class ArmadaController extends Controller
{
    public function armada()
    {
        $armada = Armada::with('mobil')->paginate(10);
        $mobil = Mobil::all();
        return view('admin.armada', compact('armada', 'mobil'));
    }

    public function list_armada(Request $request)
    {
        $armada = Armada::query()
            ->leftjoin('mobil', 'armada_mobil.id_mobil', '=', 'mobil.id')
            ->select([
                'armada_mobil.id',
                'mobil.nama_mobil as nama_mobil',
                'plat_nomor',
                'status_mobil',
                'armada_mobil.created_at',
            ])
            ->latest()
            ->get();

        $no = 0;
        $data = array();
        foreach ($armada as $ard) {
            $actions = "<div class=\"actions\">
                    <a href=\"\" data-toggle=\"modal\" data-target=\"#modalEdit" . $ard->id . "\" class=\"btn btn-tertiary\"><i class=\"fas fa-pencil-alt\" style=\"color: white;\"></i></a>
                    <a href=\"\" data-toggle=\"modal\" data-target=\"#modalDelete" . $ard->id . "\" class=\"btn btn-danger\"><i class=\"delete-row\"><i class=\"far fa-trash-alt\" style=\"color: white;\"></i></a>
                </div>";


            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $ard->nama_mobil;
            $row[] = $ard->plat_nomor;
            $row[] = $ard->status_mobil;
            $row[] = $actions;
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }



    public function tambah_armada(Request $request)
    {
        $request->validate([
            'plat' => 'required|min:3',
        ]);

        Armada::create([
            'id_mobil' => $request->id,
            'plat_nomor' => $request->plat,
            'status_mobil' => 'tersedia'
        ]);

        return redirect('/admin/home/armada');
    }

    public function edit_armada(Request $request, $id)
    {
        $request->validate([
            'plat' => 'required|min:3',
        ]);

        $update_armada = Armada::find($id);
        $update_armada->plat_nomor = $request->plat;
        $update_armada->save();
        return redirect('/admin/home/armada');
    }

    public function delete_armada($id)
    {
        Armada::destroy($id);
        return redirect('/admin/home/armada');
    }
}
