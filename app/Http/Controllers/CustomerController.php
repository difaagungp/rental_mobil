<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function customer()
    {
        $cust = User::where('is_admin', '0')->paginate(10);
        return view('admin.customer', compact('cust'));
    }

    public function list_customer(Request $request)
    {
        $user = User::query()
            ->select([
                'id',
                'name',
                'email',
                'ktp',
                'nomor_telepon',
            ])
            ->where('is_admin', '0')
            ->latest()
            ->get();

        $no = 0;
        $data = array();
        foreach ($user as $usr) {
            $actions = "<div class=\"actions\">
                    <a href=\"\" data-toggle=\"modal\" data-target=\"#modalEdit" . $usr->id . "\" class=\"btn btn-tertiary\"><i class=\"fas fa-pencil-alt\" style=\"color: white;\"></i></a>
                    <a href=\"\" data-toggle=\"modal\" data-target=\"#modalDelete" . $usr->id . "\" class=\"btn btn-danger\"><i class=\"delete-row\"><i class=\"far fa-trash-alt\" style=\"color: white;\"></i></a>
                </div>";


            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $usr->name;
            $row[] = $usr->email;
            $row[] = $usr->ktp;
            $row[] = $usr->nomor_telepon;
            $row[] = $actions;
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function edit_cust(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|max:255|min:3',
            'email' => 'required|min:3|email',
            'ktp' => 'required|min:3',
            'telp' => 'required|min:3',
            'alamat' => 'required|min:3'
        ]);

        $update_cust = User::find($id);
        $update_cust->name = $request->nama;
        $update_cust->email = $request->email;
        $update_cust->ktp = $request->ktp;
        $update_cust->nomor_telepon = $request->telp;
        $update_cust->alamat = $request->alamat;
        $update_cust->save();
        return redirect('/admin/home/customer');
    }

    public function delete_cust($id)
    {
        User::destroy($id);
        return redirect('/admin/home/customer');
    }
}
