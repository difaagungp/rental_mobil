<?php

namespace App\Http\Controllers;

use App\Models\Driver;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    public function driver()
    {
        $driver = Driver::paginate(10);
        return view('admin.driver', compact('driver'));
    }

    public function list_driver(Request $request)
    {
        $driver = Driver::query()
            ->select([
                'id',
                'nama_driver',
                'umur_driver',
                'alamat_driver',
                'telp_driver',
                'status_driver',
            ])
            ->latest()
            ->get();

        $no = 0;
        $data = array();
        foreach ($driver as $drv) {
            $actions = "<div class=\"actions\">
                    <a href=\"\" data-toggle=\"modal\" data-target=\"#modalEdit" . $drv->id . "\" class=\"btn btn-tertiary\"><i class=\"fas fa-pencil-alt\" style=\"color: white;\"></i></a>
                    <a href=\"\" data-toggle=\"modal\" data-target=\"#modalDelete" . $drv->id . "\"class=\"btn btn-danger\"><i class=\"delete-row\"><i class=\"far fa-trash-alt\" style=\"color: white;\"></i></a>
                </div>";


            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $drv->nama_driver;
            $row[] = $drv->umur_driver;
            $row[] = $drv->alamat_driver;
            $row[] = $drv->telp_driver;
            $row[] = $drv->status_driver;
            $row[] = $actions;
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function edit_driver(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|max:255|min:3',
            'umur' => 'required|min:1',
            'alamat' => 'required|min:3',
            'telp' => 'required|min:3'
        ]);

        $update_driver = Driver::find($id);
        $update_driver->nama_driver = $request->nama;
        $update_driver->umur_driver = $request->umur;
        $update_driver->alamat_driver = $request->alamat;
        $update_driver->telp_driver = $request->telp;
        $update_driver->save();
        return redirect('/admin/home/driver');
    }

    public function delete_driver($id)
    {
        Driver::destroy($id);
        return redirect('/admin/home/driver');
    }

    public function tambah_driver(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:255|min:3',
            'umur' => 'required|min:1',
            'alamat' => 'required|min:3',
            'telp' => 'required|min:3'
        ]);

        Driver::create([
            'nama_driver' => $request->nama,
            'umur_driver' => $request->umur,
            'alamat_driver' => $request->alamat,
            'telp_driver' => $request->telp,
            'status_driver' => 'Tersedia'
        ]);

        return redirect('/admin/home/driver');
    }
}
