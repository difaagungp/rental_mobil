<?php

namespace App\Http\Controllers;


use App\Models\Mobil;
use App\Models\Driver;
use App\Models\Peminjaman;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $mobil = Mobil::all();
        return view('user.home', compact('mobil'));
    }

    public function adminHome()
    {
        $count_order = Peminjaman::all()->count();
        $count_money = Peminjaman::where('status_sewa', 'Dikembalikan')->pluck('harga_sewa')->sum();
        $count_car = Mobil::pluck('armada_mobil')->sum();
        $count_driver = Driver::all()->count();
        $count_user = User::where('is_admin', '0')->count();
        if (auth()->user()->is_admin === 1) {
            $coba = auth()->user()->unreadNotifications;
        }
        return view('admin.adminHome', compact('count_order', 'count_money', 'count_user', 'count_car', 'count_driver', 'coba'));
    }
}
