<?php

namespace App\Http\Controllers;

use App\Models\Armada;
use App\Models\Mobil;
use Illuminate\Http\Request;

class MobilController extends Controller
{
    public function mobil()
    {
        $mobil = Mobil::paginate(10);
        return view('admin.mobil', compact('mobil'));
    }

    public function list_mobil(Request $request)
    {
        $mobil = Mobil::query()
            ->select([
                'id',
                'nama_mobil',
                'jenis_mobil',
                'merk_mobil',
                'harga_mobil',
                'armada_mobil',
            ])
            ->latest()
            ->get();

        $no = 0;
        $data = array();
        foreach ($mobil as $mbl) {
            $actions = "<div class=\"actions\">
                    <a href=\"\" data-toggle=\"modal\" data-target=\"#modalEdit" . $mbl->id . "\" class=\"btn btn-tertiary\"><i class=\"fas fa-pencil-alt\" style=\"color: white;\"></i></a>
                    <a href=\"\" data-toggle=\"modal\" data-target=\"#modalDelete" . $mbl->id . "\" class=\"btn btn-danger\"><i class=\"delete-row\"><i class=\"far fa-trash-alt\" style=\"color: white;\"></i></a>
                </div>";


            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $mbl->nama_mobil;
            $row[] = $mbl->jenis_mobil;
            $row[] = $mbl->merk_mobil;
            $row[] = $mbl->harga_mobil;
            $row[] = $mbl->armada_mobil;
            $row[] = $actions;
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function edit_mobil(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|max:255|min:3',
            'merk' => 'required|min:3',
            'jenis' => 'required|min:3',
            'harga' => 'required|min:3',
            'jumlah' => 'required|max:3',
            'bahanbakar' => 'required|min:3',
            'gambar' => 'mimes:jpeg,png,jpg|max:2048'
        ]);

        if ($request->hasFile('gambar')) {
            $nama_gambar = time() . "_" . $request->gambar->getClientOriginalName();

            $request->gambar->move(public_path('img'), $nama_gambar);
            $update_mobil = Mobil::find($id);
            $update_mobil->nama_mobil = $request->nama;
            $update_mobil->jenis_mobil = $request->jenis;
            $update_mobil->merk_mobil = $request->merk;
            $update_mobil->harga_mobil = $request->harga;
            $update_mobil->armada_mobil = $request->jumlah;
            $update_mobil->bahanbakar_mobil = $request->bahanbakar;
            $update_mobil->gambar_mobil = $nama_gambar;
            $update_mobil->save();
            return redirect('/admin/home/mobil');
        } else {
            $update_mobil = Mobil::find($id);
            $update_mobil->nama_mobil = $request->nama;
            $update_mobil->jenis_mobil = $request->jenis;
            $update_mobil->merk_mobil = $request->merk;
            $update_mobil->harga_mobil = $request->harga;
            $update_mobil->armada_mobil = $request->jumlah;
            $update_mobil->bahanbakar_mobil = $request->bahanbakar;
            $update_mobil->save();
            return redirect('/admin/home/mobil');
        }
    }

    public function delete_mobil($id)
    {
        Mobil::destroy($id);
        Armada::destroy('id_mobil', $id);
        return redirect('/admin/home/mobil');
    }

    public function tambah_mobil(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:255|min:3',
            'merk' => 'required|min:3',
            'jenis' => 'required|min:3',
            'harga' => 'required|min:3',
            'jumlah' => 'required|max:3',
            'bahanbakar' => 'required|min:3',
            'gambar' => 'mimes:jpeg,png,jpg|max:2048'
        ]);

        if ($request->hasFile('gambar')) {
            $nama_gambar = time() . "_" . $request->gambar->getClientOriginalName();

            $request->gambar->move(public_path('img'), $nama_gambar);

            Mobil::create([
                'nama_mobil' => $request->nama,
                'jenis_mobil' => $request->jenis,
                'merk_mobil' => $request->merk,
                'harga_mobil' => $request->harga,
                'armada_mobil' => $request->jumlah,
                'bahanbakar_mobil' => $request->bahanbakar,
                'gambar_mobil' => $nama_gambar
            ]);
        } else {
            Mobil::create([
                'nama_mobil' => $request->nama,
                'jenis_mobil' => $request->jenis,
                'merk_mobil' => $request->merk,
                'harga_mobil' => $request->harga,
                'armada_mobil' => $request->jumlah,
                'bahanbakar_mobil' => $request->bahanbakar
            ]);
        }


        return redirect('/admin/home/mobil');
    }
}
