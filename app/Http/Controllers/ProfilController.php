<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ProfilController extends Controller
{
    public function profile()
    {
        $profil = User::where('id', auth()->user()->id)->first();
        return view('user.profile', compact('profil'));
    }

    public function edit_profil()
    {
        $profil = User::where('id', auth()->user()->id)->first();
        return view('user.edit_profile', compact('profil'));
    }
}
