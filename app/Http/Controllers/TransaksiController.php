<?php

namespace App\Http\Controllers;

use App\Models\Armada;
use App\Models\Driver;
use App\Models\Mobil;
use App\Notifications\NewSewaNotif;
use App\Models\Peminjaman;
use App\Models\Tagihan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class TransaksiController extends Controller
{
    public function list_sewa()
    {
        $pinjam = Peminjaman::with('user', 'mobil', 'tagihan')->where('status_sewa', 'Proses')->paginate(10);
        return view('admin.peminjaman', compact('pinjam'));
    }

    public function sewa_list(Request $request)
    {
        $proses = Peminjaman::query()
            ->leftjoin('users', 'peminjaman.user_id', '=', 'users.id')
            ->leftjoin('mobil', 'peminjaman.id_mobil', '=', 'mobil.id')
            ->leftjoin('tagihan', 'peminjaman.id_tagihan', '=', 'tagihan.id_tagihan')
            ->select([
                'peminjaman.id as id',
                'users.name as name',
                'mobil.nama_mobil as mobil',
                'tipe_sewa',
                'waktu_sewa',
                'tanggal_sewa',
                'harga_sewa',
                'status_sewa',
                'tagihan.id_tagihan as tagihan',
                'peminjaman.created_at',
            ])
            ->where('status_sewa', 'Proses')
            ->latest()
            ->get();

        $no = 0;
        $data = array();
        foreach ($proses as $prs) {
            $actions = "<div class=\"actions\">
            <a href=\"/admin/home/peminjaman/confirm/" . $prs->tagihan . "\" class=\"btn btn-primary\"><i class=\"fas fa-check\" style=\"color: white;\"></i></a>   
                </div>";


            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $prs->name;
            $row[] = $prs->mobil;
            $row[] = $prs->tipe_sewa;
            $row[] = $prs->waktu_sewa;
            $row[] = $prs->tanggal_sewa;
            $row[] = $prs->harga_sewa;
            $row[] = $prs->status_sewa;
            $row[] = $actions;
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }
    public function list_disewa()
    {
        $pinjam = Peminjaman::with('user', 'mobil', 'driver', 'armada')->where('status_sewa', 'Disewa')->paginate(10);
        return view('admin.disewa', compact('pinjam'));
    }

    public function disewa_list(Request $request)
    {
        $proses = Peminjaman::query()
            ->leftjoin('users', 'peminjaman.user_id', '=', 'users.id')
            ->leftjoin('mobil', 'peminjaman.id_mobil', '=', 'mobil.id')
            ->select([
                'peminjaman.id as id',
                'users.name as name',
                'mobil.nama_mobil as mobil',
                'tipe_sewa',
                'waktu_sewa',
                'tanggal_sewa',
                'harga_sewa',
                'status_sewa',
                'peminjaman.created_at',
            ])
            ->where('status_sewa', 'Disewa')
            ->latest()
            ->get();

        $no = 0;
        $data = array();
        foreach ($proses as $prs) {
            $actions = "<div class=\"actions\">
                    <a href=\"\" data-toggle=\"modal\" data-target=\"#modalEdit" . $prs->id . "\" class=\"btn btn-primary\"><i class=\"fas fa-check\" style=\"color: white;\"></i></a>
                </div>";


            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $prs->name;
            $row[] = $prs->mobil;
            $row[] = $prs->tipe_sewa;
            $row[] = $prs->waktu_sewa;
            $row[] = $prs->tanggal_sewa;
            $row[] = $prs->harga_sewa;
            $row[] = $prs->status_sewa;
            $row[] = $actions;
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function list_selesai()
    {
        $pinjam = Peminjaman::with('user', 'mobil', 'driver', 'armada')->where('status_sewa', 'Dikembalikan')->paginate(10);
        return view('admin.selesai_sewa', compact('pinjam'));
    }

    public function selesai_list(Request $request)
    {
        $proses = Peminjaman::query()
            ->leftjoin('users', 'peminjaman.user_id', '=', 'users.id')
            ->leftjoin('mobil', 'peminjaman.id_mobil', '=', 'mobil.id')
            ->select([
                'peminjaman.id as id',
                'users.name as name',
                'mobil.nama_mobil as mobil',
                'tipe_sewa',
                'waktu_sewa',
                'tanggal_sewa',
                'harga_sewa',
                'status_sewa',
                'peminjaman.created_at',
            ])
            ->where('status_sewa', 'Dikembalikan')
            ->latest()
            ->get();

        $no = 0;
        $data = array();
        foreach ($proses as $prs) {
            $actions = "<div class=\"actions\">
                    <a href=\"\" data-toggle=\"modal\" data-target=\"#modalEdit" . $prs->id . "\" class=\"btn btn-warning\"><i class=\"fas fa-book-reader\" style=\"color: white;\"></i></a>
                    
                </div>";


            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $prs->name;
            $row[] = $prs->mobil;
            $row[] = $prs->tipe_sewa;
            $row[] = $prs->waktu_sewa;
            $row[] = $prs->tanggal_sewa;
            $row[] = $prs->harga_sewa;
            $row[] = $prs->status_sewa;
            $row[] = $actions;
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function histori_sewa()
    {
        $peminjaman = Peminjaman::with('user', 'mobil', 'tagihan')->where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->paginate(10);
        $mobil = Peminjaman::with('mobil')->first();
        return view('user.pinjaman', compact('peminjaman',));
    }

    public function histori_sewa_list(Request $request)
    {
        $proses = Peminjaman::query()
            ->leftjoin('users', 'peminjaman.user_id', '=', 'users.id')
            ->leftjoin('mobil', 'peminjaman.id_mobil', '=', 'mobil.id')
            ->leftjoin('tagihan', 'peminjaman.id_tagihan', '=', 'tagihan.id_tagihan')
            ->select([
                'peminjaman.id as id',
                'mobil.nama_mobil as mobil',
                'tipe_sewa',
                'waktu_sewa',
                'tanggal_sewa',
                'harga_sewa',
                'status_sewa',
                'peminjaman.created_at',
                'tagihan.id_tagihan as tagihan',
                'tagihan.status_tagihan as status_tagihan',
            ])
            ->where('user_id', auth()->user()->id)
            ->latest()
            ->get();

        $no = 0;
        $data = array();
        foreach ($proses as $prs) {
            if ($prs->status_tagihan === 'Belum Bayar') {
                $actions = "<div class=\"actions\">
                <a href=\"/home/pinjaman/detail/" . $prs->id . "\" class=\"btn btn-primary\"><i class=\"fas fa-book-reader\" style=\"color: white;\"></i></a>
                <a href=\"\" data-toggle=\"modal\" data-target=\"#modalEdit" . $prs->tagihan . "\" class=\"btn btn-danger\"><i class=\"fas fa-money-bill-alt\" style=\"color: white;\"></i></a>
                </div>";
            } else {
                $actions = "<div class=\"actions\">
                <a href=\"/home/pinjaman/detail/" . $prs->id . "\" class=\"btn btn-primary\"><i class=\"fas fa-book-reader\" style=\"color: white;\"></i></a>
                </div>";
            }



            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $prs->mobil;
            $row[] = $prs->tipe_sewa;
            $row[] = $prs->waktu_sewa." Hari";
            $row[] = $prs->tanggal_sewa;
            $row[] = $prs->harga_sewa;
            $row[] = $prs->status_sewa;
            $row[] = $actions;
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function detail_sewa($id)
    {
        $detail = Peminjaman::with('user')->where('id', $id)->first();
        return view('user.detail_sewa', compact('detail'));
    }

    public function tagihan()
    {
        $bayar = Peminjaman::with('user', 'tagihan')->paginate(10);
        return view('admin.tagihan', compact('bayar'));
    }

    public function tagihan_list(Request $request)
    {
        $tagihan = Peminjaman::query()
            ->leftjoin('users', 'peminjaman.user_id', '=', 'users.id')
            ->leftjoin('tagihan', 'peminjaman.id_tagihan', '=', 'tagihan.id_tagihan')
            ->select([
                'tagihan.id as id',
                'tagihan.id_tagihan as id_tagihan',
                'users.name as nama',
                'harga_sewa',
                'tagihan.status_tagihan as status',
                'peminjaman.created_at',
            ])
            ->latest()
            ->get();

        $no = 0;
        $data = array();
        foreach ($tagihan as $prs) {
            if ($prs->status === 'Lunas') {
                $actions = "<div class=\"actions\">
                <a href=\"\" data-toggle=\"modal\" data-target=\"#modalEdit" . $prs->id_tagihan . "\" class=\"btn btn-danger\"><i class=\"fas fa-file-invoice-dollar\" style=\"color: white;\"></i></a>
                    </div>";
            } else {
                $actions = "<div class=\"actions\">
                <a href=\"/admin/home/peminjaman/confirm/" . $prs->id_tagihan . "\" class=\"btn btn-primary\"><i class=\"fas fa-check\" style=\"color: white;\"></i></a>
                <a href=\"\" data-toggle=\"modal\" data-target=\"#modalEdit" . $prs->id_tagihan . "\" class=\"btn btn-danger\"><i class=\"fas fa-file-invoice-dollar\" style=\"color: white;\"></i></a>
                    </div>";
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $prs->id_tagihan;
            $row[] = $prs->nama;
            $row[] = $prs->harga_sewa;
            $row[] = $prs->status;
            $row[] = $actions;
            $data[] = $row;
        }
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function update_bayar(Request $request, $id)
    {

        $bayar = Tagihan::where('id_tagihan', $id)->first();
        $nama_gambar = time() . "_" . $request->gambar->getClientOriginalName();
        $request->gambar->move(public_path('tagihan'), $nama_gambar);

        $bayar->gambar_tagihan = $nama_gambar;
        $bayar->save();

        $admin = User::where('is_admin', 1)->first();

        Notification::send($admin, new NewSewaNotif($bayar));
        return redirect('/home/pinjaman');
    }
    public function update_profil(Request $request, $id)
    {
        $user = User::where('id', $id)->first();

        $user->name = $request->nama;
        $user->ktp = $request->ktp;
        $user->nomor_telepon = $request->telp;
        $user->alamat = $request->alamat;
        $user->email = $request->email;
        $user->save();

        return redirect('/home/profile');
    }

    public function pesan($id_mobil)
    {
        $nama = User::where('id', auth()->user()->id)->first();
        $mobil = Mobil::find($id_mobil);
        return view('user.pesan', compact('mobil', 'nama'));
    }

    public function sewa(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:5',
            'tipe' => 'required',
            'hari' => 'required|min:1',
            'tanggal' => 'required',
            'total' => 'required',
        ]);
        $latestOrder = User::orderBy('created_at', 'DESC')->first();
        Peminjaman::create([
            'user_id' => $request->id,
            'id_mobil' => $request->id_mobil,
            'id_tagihan' => 'ORD-' . str_pad(time(), STR_PAD_LEFT),
            'tipe_sewa' => $request->tipe,
            'waktu_sewa' => $request->hari,
            'tanggal_sewa' => $request->tanggal,
            'harga_sewa' => $request->total,
            'status_sewa' => 'Proses'
        ]);


        Tagihan::create([
            'id_tagihan' => 'ORD-' . str_pad(time(), STR_PAD_LEFT),
            'status_tagihan' => 'Belum Bayar'
        ]);



        return redirect('/home/pinjaman');
    }

    public function confirm_sewa($id)
    {
        $supir = Driver::where('status_driver', 'tersedia')->get();
        $sewa = Peminjaman::with('mobil', 'user', 'tagihan')->where('id_tagihan', $id)->first();
        $armada = Armada::where('id_mobil', $sewa->mobil->id)->where('status_mobil', 'Tersedia')->get();
        return view('admin.confirm_sewa', compact('sewa', 'armada', 'supir'));
    }

    public function update_confirm(Request $request, $id)
    {
        $peminjaman = Peminjaman::with('mobil', 'user', 'driver', 'armada', 'tagihan')->where('id', $id)->first();
        $peminjaman->mobil->armada_mobil -= 1;
        $peminjaman->id_driver = $request->supir;
        $peminjaman->id_armada = $request->plat;
        $peminjaman->status_sewa = 'Disewa';
        $peminjaman->mobil->save();
        $peminjaman->save();

        if ($request->hasAny('supir')) {
            $armada = Armada::where('id', $peminjaman->id_armada)->first();
            $armada->status_mobil = 'Disewa';
            $armada->save();

            $supir = Driver::where('id', $peminjaman->id_driver)->first();
            $supir->status_driver = 'Jalan';
            $supir->save();

            $tagihan = Tagihan::where('id_tagihan', $peminjaman->id_tagihan)->first();
            $tagihan->status_tagihan = 'Lunas';
            $tagihan->save();
        } else {
            $armada = Armada::where('id', $peminjaman->id_armada)->first();
            $armada->status_mobil = 'Disewa';
            $armada->save();

            $tagihan = Tagihan::where('id_tagihan', $peminjaman->id_tagihan)->first();
            $tagihan->status_tagihan = 'Lunas';
            $tagihan->save();
        }

        return redirect('/admin/home/disewa');
    }

    public function update_disewa($id)
    {
        $peminjaman = Peminjaman::with('mobil', 'user', 'driver', 'armada')->where('id', $id)->first();
        $peminjaman->mobil->armada_mobil += 1;
        $peminjaman->status_sewa = 'Dikembalikan';
        $peminjaman->mobil->save();
        $peminjaman->save();

        if ($peminjaman->id_driver == "") {
            $armada = Armada::where('id', $peminjaman->id_armada)->first();
            $armada->status_mobil = 'Tersedia';
            $armada->save();
        } else {
            $armada = Armada::where('id', $peminjaman->id_armada)->first();
            $armada->status_mobil = 'Tersedia';
            $armada->save();

            $supir = Driver::where('id', $peminjaman->id_driver)->first();
            $supir->status_driver = 'Tersedia';
            $supir->save();
        }

        return redirect('/admin/home/selesai');
    }

    public function markNotification(Request $request, $id, $id_tagihan)
    {
        auth()->user()
            ->unreadNotifications
            ->when($id, function ($query) use ($id) {
                return $query->where('id', $id);
            })
            ->markAsRead();

        return redirect()->route('confirm', $id_tagihan);
    }
}
