<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Armada extends Model
{
    protected $table = 'armada_mobil';
    protected $guarded = ['id'];

    public function mobil()
    {
        return $this->hasOne('App\Models\Mobil', 'id', 'id_mobil');
    }
}
