<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mobil extends Model
{
    protected $table = 'mobil';
    protected $guarded = ['id'];

    protected $fillable = [
        'nama_mobil',
        'jenis_mobil',
        'merk_mobil',
        'harga_mobil',
        'armada_mobil',
        'bahanbakar_mobil',
        'gambar_mobil'
    ];

    public function armadas()
    {
        return $this->hasMany('App\Models\Armada', 'id', 'id_mobil');
    }
}
