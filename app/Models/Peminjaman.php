<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = 'peminjaman';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    public function mobil()
    {
        return $this->hasOne('App\Models\Mobil', 'id', 'id_mobil');
    }
    public function driver()
    {
        return $this->hasOne('App\Models\Driver', 'id', 'id_driver');
    }
    public function armada()
    {
        return $this->hasOne('App\Models\Armada', 'id', 'id_armada');
    }
    public function tagihan()
    {
        return $this->hasOne('App\Models\Tagihan', 'id_tagihan', 'id_tagihan');
    }
}
