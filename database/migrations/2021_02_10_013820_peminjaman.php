<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Peminjaman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peminjaman', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('id_mobil')->nullable();
            $table->integer('id_armada')->nullable();
            $table->integer('id_driver')->nullable();
            $table->string('id_tagihan')->nullable();
            $table->string('tipe_sewa');
            $table->string('waktu_sewa');
            $table->string('tanggal_sewa');
            $table->string('harga_sewa');
            $table->string('status_sewa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
