<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateArmada extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('armada_mobil')->insert([
            'id' => '1',
            'id_mobil' => '1',
            'plat_nomor' => 'AD 2412 CC',
            'status_mobil' => 'Tersedia'
        ]);
    }
}
