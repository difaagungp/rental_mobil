<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateDriver extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('driver')->insert([
            'id' => '1',
            'nama_driver' => 'Tukimin',
            'umur_driver' => '42',
            'alamat_driver' => 'Magelang',
            'telp_driver' => '0987788678',
            'status_driver' => 'Tersedia'
        ]);
    }
}
