<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateMobil extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mobil')->insert([
            'id' => '1',
            'nama_mobil' => 'Avanza',
            'jenis_mobil' => 'MPV',
            'merk_mobil' => 'Toyota',
            'harga_mobil' => '200000',
            'armada_mobil' => '2',
            'bahanbakar_mobil' => 'Pertalite',
            'gambar_mobil' => 'avanza.jpg'
        ]);
    }
}
