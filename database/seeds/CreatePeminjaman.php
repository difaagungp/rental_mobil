<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreatePeminjaman extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('peminjaman')->insert([
            'id' => '1',
            'user_id' => '2',
            'id_mobil' => '0',
            'id_armada' => '0',
            'id_driver' => '0',
            'tipe_sewa' => 'hanya mobil',
            'waktu_sewa' => '3',
            'tanggal_sewa' => '2021-02-10',
            'harga_sewa' => '800000',
            'status_sewa' => 'proses'
        ]);
    }
}
