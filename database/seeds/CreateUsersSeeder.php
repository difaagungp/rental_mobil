<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'Admin',
                'ktp' => '0987656781245678',
                'nomor_telepon' => '0987656291898',
                'alamat' => 'Solo',
                'email' => 'admin@coba.com',
                'is_admin' => '1',
                'password' => bcrypt('123456'),
            ],
            [
                'name' => 'Difa Agung',
                'ktp' => '09876567876545678',
                'nomor_telepon' => '0987656777898',
                'alamat' => 'Klaten',
                'email' => 'user@coba.com',
                'is_admin' => '0',
                'password' => bcrypt('123456'),
            ],
        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
