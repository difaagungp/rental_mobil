$(".tambah-armada").on("submit", function (e) {
    const create_btn = $(".btn-tambah");
    e.preventDefault();
    create_btn.prop("disabled", true);
    create_btn.html(
        `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Memproses`
    );
    $.ajax({
        url: this.action,
        type: this.method,
        data: $(this).serialize(),
    })
        .done(function (res) {
            new PNotify({
                title: "Berhasil!",
                text: "Armada berhasil disimpan ke basis data.",
                addclass: "icon-nb",
                width: "340px",
                icon: false,
                type: "success",
                animate_speed: "fast",
            });
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        })
        .fail(function (res) {
            let errorList = "";
            if (typeof (res.responseJSON.errors) === 'object') {
                $.each(res.responseJSON.errors, function (key, value) {
                    if (value.length > 1) {
                        $.each(value, function (key, value) {
                            errorList += value + "<br/>";
                        });
                    } else {
                        errorList += value + "<br/>";
                    }
                });
            } else {
                errorList += "Data armada gagal disimpan ke dalam basis data.";
            }
            new PNotify({
                title: "<strong>Gagal!</strong>",
                text: errorList,
                icon: false,
                width: "340px",
                type: "error",
                animate_speed: "fast",
            });
            create_btn.prop("disabled", false);
            create_btn.html("Simpan");
            if (res.responseJSON?.status === "session_expired") {
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            }
        });
});
