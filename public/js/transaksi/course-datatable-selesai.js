$(document).ready(function () {
    var $table = $(".pinjam");
    var table = $table.DataTable({
        sDom:
            '<"text-right mb-md"T><"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>pr',
        "language": {
            "processing": '<div class="d-flex justify-content-center align-items-center tbr_dt_spinner"><div class="spinner-border text-light" role="status"></div> Sedang memproses</div>',
            "loadingRecords": "",
            "search": '',
            "searchPlaceholder": "Search",
        },
        processing: true,
        columnDefs: [{ orderable: false, targets: [8] }],
        ajax: {
            url: "/admin/home/selesai/list",
            type: "GET",

            error: function (res) {
                let errorList = "";
                if (typeof (res.responseJSON.message) === "object") {
                    $.each(res.responseJSON.message, function (key, value) {
                        if (value.length > 1) {
                            $.each(value, function (key, value) {
                                errorList += value + "<br/>";
                            });
                        } else {
                            errorList += value + "<br/>";
                        }
                    });
                } else {
                    errorList += "Terjadi kesalahan pada sistem. Silahkan reload ulang halaman ini.";
                }
                new PNotify({
                    title: "<strong>Gagal!</strong>",
                    text: errorList,
                    icon: false,
                    width: "340px",
                    type: "error",
                    animate_speed: "fast",
                });
                if (res.responseJSON?.status === "session_expired") {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
            },
        },
    });
});
