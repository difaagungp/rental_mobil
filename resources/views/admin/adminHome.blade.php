@extends('admin.app')

@section('content')
<header class="page-header">
    <h2>Home Admin</h2>
</header>

<div class="card">
    @foreach($coba as $notif)
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong> {{$notif->data['text']}} </strong>
        <form class="d-inline" method="POST" action="{{ route('markNotification', [$notif->id, $notif->data['id_tagihan']])}}">
            @csrf
            <button type="submit" class="btn btn-link" id="mark-as-read">Cek Disini!</button>.
        </form>
    </div>
    @endforeach
</div>

<div class="row">
    <div class="col-xl-6">
        <section class="card card-featured-left card-featured-tertiary mb-3">
            <div class="card-body">

                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-tertiary">
                            <i class="fas fa-shopping-cart"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Total Penyewaan Mobil</h4>
                            <div class="info">
                                <strong class="amount">{{$count_order}}</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-xl-6">
        <section class="card card-featured-left card-featured-secondary mb-3">
            <div class="card-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-secondary">
                            <i class="fas fa-dollar-sign"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Total Pendapatan</h4>
                            <div class="info">
                                <strong class="amount">@currency($count_money)</strong>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-xl-6">
        <section class="card card-featured-left card-featured-primary mb-3">
            <div class="card-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-primary">
                            <i class="fas fa-user"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Total Pelanggan</h4>
                            <div class="info">
                                <strong class="amount">{{$count_user}}</strong>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-xl-6">
        <section class="card card-featured-left card-featured-warning mb-3">
            <div class="card-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-warning">
                            <i class="fas fa-car-side"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Total Mobil</h4>
                            <div class="info">
                                <strong class="amount">{{$count_car}}</strong>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-xl-6">
        <section class="card card-featured-left card-featured-success mb-3">
            <div class="card-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-success">
                            <i class="fas fa-id-card"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Total Driver</h4>
                            <div class="info">
                                <strong class="amount">{{$count_driver}}</strong>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>


@endsection