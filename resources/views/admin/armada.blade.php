@extends('admin.app')
@section('blockhead')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
<link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
@endsection
@section('content')
<header class="page-header">
    <h2>Armada Mobil</h2>
</header>

<div class="card-body">
    <button class="btn btn-primary" style="margin-bottom: 1rem;" data-toggle="modal" data-target="#modalTambah">Tambah Armada</button>
    <table class="table table-responsive-md mb-0 armada">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama Mobil</th>
                <th>Plat Nomor</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<!-- modalcreate -->
<div class="modal fade" id="modalTambah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Armada</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{route('tambah_armada')}}" method="POST" class="tambah-armada">
                    @csrf
                    <form>
                        <div class="form-group">
                            <label for="title">Mobil</label>
                            <select class="form-control mb-3" id="id" name="id">
                                @foreach ($mobil as $ms)
                                <option value="{{$ms->id}}">{{$ms->nama_mobil}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputJenis">Plat Nomor</label>
                            <input type="text" class="form-control" id="plat" name="plat" placeholder="Plat Nomor">
                            @error('plat')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary btn-tambah" type="submit">Add</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

<!-- modaledit -->
@foreach ($armada as $ar)
<div class="modal fade" id="modalEdit{{$ar->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Armada</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/admin/home/armada/{{$ar->id}}" method="POST" class="edit-armada">
                    @csrf
                    @method('PUT')
                    <form>
                        <div class="form-group">
                            <label for="inputJenis">Plat Nomor</label>
                            <input type="text" class="form-control" id="plat" name="plat" placeholder="Plat Nomor" value="{{$ar->plat_nomor}}">
                            @error('plat')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary btn-edit" type="submit">Save changes</button>
                        </div>
                    </form>
            </div>

        </div>
    </div>
</div>
@endforeach

<!-- modaldelete -->
@foreach ($armada as $ar)
<div class="modal fade" id="modalDelete{{$ar->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Delete Driver</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Apakah anda yakin menghapus Armada ini? : <span>{{$ar->plat_nomor}} </span></h4>
            </div>
            <div class="modal-footer">
                <form action="/admin/home/armada/{{$ar->id}}" method="POST" class="delete-armada">
                    @csrf
                    @method('delete')
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-delete-armada">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection

@section('blockfoot')
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{asset('js/armada/course-create.js')}}"></script>
<script src="{{asset('js/armada/course-update.js')}}"></script>
<script src="{{asset('js/armada/course-delete.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/armada/course-datatable.js') }}"></script>
@endsection