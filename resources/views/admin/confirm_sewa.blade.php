@extends('admin.app')

@section('content')
<header class="page-header">
    <h2>Konfirmasi Penyewaan</h2>
</header>

<div class="container">
    <div class="row">
        <div class="col">
            <div class="card border-0 shadow-0">
                <div class="card-header border-0" style="width: 18rem;">
                    <div class="card-title">Informasi Sewa</div>
                </div>
                <div class="card-body" style="width: 18rem;">
                    <p><strong>Bukti Pembayaran</strong></p>
                    <img src="{{asset('tagihan/'.$sewa->tagihan->gambar_tagihan)}}" alt="Penyewa Belum Upload Bukti Bayar" style="width: 15rem;">
                    <table class="table-borderless">
                        <tbody>
                            <tr>
                                <td class="text-muted w-50">Nama Penyewa</td>
                                <th class="text-right">{{$sewa->user->name}}</th>
                            </tr>
                            <tr>
                                <td class="text-muted w-50">Nama Mobil</td>
                                <th class="text-right">{{$sewa->mobil->nama_mobil}}</th>
                            </tr>
                            <tr>
                                <td class="text-muted w-50">Mulai Sewa</td>
                                <th class="text-right">{{$sewa->tanggal_sewa}}</th>
                            </tr>
                            <tr>
                                <td class="text-muted w-50">Lama Sewa</td>
                                <th class="text-right">{{$sewa->waktu_sewa}} Hari</th>
                            </tr>
                            <tr>
                                <td class="text-muted w-50">Tipe Peminjaman</td>
                                <th class="text-right" id="tipe">{{$sewa->tipe_sewa}}</th>
                            </tr>
                            <tr>
                                <td class="text-muted w-50">Status</td>
                                <th class="text-right">{{$sewa->status_sewa}}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="card border-0 shadow-0">
                <div class="card-header border-0">
                    <div class="card-title">Detail Sewa</div>
                </div>
                <div class="card-body">
                    <form action="/admin/home/peminjaman/{{$sewa->id}}" method="POST" class="edit-trans">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="title">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" value="{{$sewa->mobil->nama_mobil}}" readonly>
                            @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="title">Pilih Armada</label>
                            <select class="form-control mb-3" id="plat" name="plat">
                                <option value="" selected>Pilih Armada</option>
                                @foreach($armada as $ar)
                                <option value="{{$ar->id}}">{{$ar->plat_nomor}}</option>
                                @endforeach
                            </select>
                        </div>
                        @if($sewa->tipe_sewa === "mobil dan supir")
                        <div class="form-group">
                            <label for="title">Pilih Supir</label>
                            <select class="form-control mb-3" id="supir" name="supir">
                                <option value="" selected>Pilih Driver</option>
                                @foreach($supir as $sp)
                                <option value="{{$sp->id}}">{{$sp->nama_driver}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        <button type="submit" class="btn btn-primary btn-edit" style="margin-top: 1rem;">Pesan Sekarang</button>

                    </form>
                </div>


            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#jumlah").keyup(function() {
            var jumlah = $("#jumlah").val();

            var total = parseInt(jumlah) - 1;
            $("#total").val(total);
            console.log(jumlah);
        });
    });
</script>
@endsection

@section('blockfoot')
<script src="{{asset('js/transaksi/course-update.js')}}"></script>
@endsection