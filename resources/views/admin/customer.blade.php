@extends('admin.app')
@section('blockhead')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
<link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
@endsection
@section('content')
<header class="page-header">
    <h2>Customer</h2>
</header>


<div class="col">
    <section class="card">
        <div class="card-body">
            <table class="table table-responsive-md mb-0 customer" id="datatable-default">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Pelanggan</th>
                        <th>Email</th>
                        <th>NIK KTP</th>
                        <th>Nomor Telepon</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </section>
</div>

@foreach ($cust as $cs)
<div class="modal fade" id="modalEdit{{$cs->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Customer</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/admin/home/customer/{{$cs->id}}" method="POST" class="edit-cust">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{$cs->name}}">
                        @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="title">Email</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{$cs->email}}">
                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="title">NIK KTP</label>
                        <input type="text" class="form-control" id="ktp" name="ktp" value="{{$cs->ktp}}">
                        @error('ktp')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="title">Nomor Telepon</label>
                        <input type="text" class="form-control" id="telp" name="telp" value="{{$cs->nomor_telepon}}">
                        @error('telp')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="title">Alamat</label>
                        <input type="text" class="form-control" id="alamat" name="alamat" value="{{$cs->alamat}}">
                        @error('alamat')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary btn-edit" type="submit">Save changes</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endforeach

<!-- modaldelete -->
@foreach ($cust as $cs)
<div class="modal fade" id="modalDelete{{$cs->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Delete Customer</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Apakah anda yakin menghapus customer ini? : <span>{{$cs->name}} </span></h4>
            </div>
            <div class="modal-footer">
                <form action="/admin/home/customer/{{$cs->id}}" method="POST" class="delete-customer">
                    @csrf
                    @method('delete')
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-delete-customer">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection

@section('blockfoot')
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{asset('js/customer/course-update.js')}}"></script>
<script src="{{asset('js/customer/course-delete.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/customer/course-datatable.js') }}"></script>
@endsection