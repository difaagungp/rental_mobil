@extends('admin.app')
@section('blockhead')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
<link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
@endsection
@section('content')
<header class="page-header">
    <h2>List Transaksi Yang Belum Selesai</h2>
</header>

<!-- start: page -->
<div class="card-body">
    <table class="table table-responsive-md mb-0 pinjam">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama Penyewa</th>
                <th>Nama Mobil</th>
                <th>Tipe Sewa</th>
                <th>Waktu Sewa</th>
                <th>Tanggal Sewa</th>
                <th>Total Harga</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

@foreach ($pinjam as $pj)
<div class="modal fade" id="modalEdit{{$pj->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pengembalian</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/admin/home/disewa/{{$pj->id}}" method="POST" class="kembali-trans">
                    @csrf
                    @method('PUT')
                    <form>
                        <div class="form-group">
                            <label class="InputName">Nama Penyewa</label>
                            <input type="text" name="nama" placeholder="Nama Penyewa" id="nama" class="form-control" value="{{$pj->user->name}}" readonly>
                            @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputJenis">Nama Mobil</label>
                            <input type="text" class="form-control" id="mobil" name="mobil" placeholder="Nama Mobil" value="{{$pj->mobil->nama_mobil}}" readonly>
                            @error('umur')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Plat Nomor</label>
                            <input type="text" class="form-control" id="plat" name="plat" placeholder="Plat Nomor" value="{{$pj->armada->plat_nomor}}" readonly>
                            @error('alamat')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Tanggal sewa</label>
                            <input type="text" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal Sewa" value="{{$pj->tanggal_sewa}}" readonly>
                            @error('telp')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Tipe Peminjaman</label>
                            <input type="text" class="form-control" id="tipe" name="tipe" placeholder="Tipe Peminjaman" value="{{$pj->tipe_sewa}}" readonly>
                            @error('telp')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Status</label>
                            <input type="text" class="form-control" id="status" name="status" placeholder="Status" value="{{$pj->status_sewa}}" readonly>
                            @error('telp')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary btn-edit" type="submit">Return</button>
                        </div>
                    </form>
            </div>

        </div>
    </div>
</div>
@endforeach
@endsection

@section('blockfoot')
<script>
    $('.peminjaman').addClass('nav-active');
    $('.peminjaman').addClass('nav-expanded');
    $('.disewa').addClass('nav-active');
</script>
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{asset('js/transaksi/course-kembali.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/transaksi/course-datatable-disewa.js') }}"></script>
@endsection