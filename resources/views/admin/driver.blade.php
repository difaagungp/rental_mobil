@extends('admin.app')
@section('blockhead')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
<link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
@endsection
@section('content')
<header class="page-header">
    <h2>List Driver</h2>
</header>

<!-- start: page -->
<div class="card-body">
    <button class="btn btn-primary" style="margin-bottom: 1rem;" data-toggle="modal" data-target="#modalTambah">Tambah Driver</button>
    <table class="table table-responsive-md mb-0 driver">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama Driver</th>
                <th>Umur Driver</th>
                <th>Alamat Driver</th>
                <th>Telepon Driver</th>
                <th>Status Driver</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<!-- modalcreate -->
<div class="modal fade" id="modalTambah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Driver</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{route('tambah_driver')}}" method="POST" class="tambah-driver">
                    @csrf
                    <form>
                        <div class="form-group">
                            <label class="InputName">Nama Driver</label>
                            <input type="text" name="nama" placeholder="Nama Driver" id="nama" class="form-control">
                            @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputJenis">Umur Driver</label>
                            <input type="text" class="form-control" id="umur" name="umur" placeholder="Umur Driver">
                            @error('umur')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Alamat Driver</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat Driver">
                            @error('alamat')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Nomor Telepon Driver</label>
                            <input type="text" class="form-control" id="telp" name="telp" placeholder="Nomor Telepon Driver">
                            @error('telp')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary btn-tambah" type="submit">Save changes</button>
                        </div>
                    </form>
            </div>

        </div>
    </div>
</div>

<!-- modaledit -->
@foreach ($driver as $dr)
<div class="modal fade" id="modalEdit{{$dr->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Driver</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/admin/home/driver/{{$dr->id}}" method="POST" class="edit-driver">
                    @csrf
                    @method('PUT')
                    <form>
                        <div class="form-group">
                            <label class="InputName">Nama Driver</label>
                            <input type="text" name="nama" placeholder="Nama Driver" id="nama" class="form-control" value="{{$dr->nama_driver}}">
                            @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputJenis">Umur Driver</label>
                            <input type="text" class="form-control" id="umur" name="umur" placeholder="Umur Driver" value="{{$dr->umur_driver}}">
                            @error('umur')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Alamat Driver</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat Driver" value="{{$dr->alamat_driver}}">
                            @error('alamat')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Nomor Telepon Driver</label>
                            <input type="text" class="form-control" id="telp" name="telp" placeholder="Nomor Telepon Driver" value="{{$dr->telp_driver}}">
                            @error('telp')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary btn-edit" type="submit">Save changes</button>
                        </div>
                    </form>
            </div>

        </div>
    </div>
</div>
@endforeach

<!-- modaldelete -->
@foreach ($driver as $dr)
<div class="modal fade" id="modalDelete{{$dr->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Delete Driver</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Apakah anda yakin menghapus driver ini? : <span>{{$dr->nama_driver}} </span></h4>
            </div>
            <div class="modal-footer">
                <form action="/admin/home/driver/{{$dr->id}}" method="POST" class="delete-driver">
                    @csrf
                    @method('delete')
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-delete-driver">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection

@section('blockfoot')
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{asset('js/driver/course-create.js')}}"></script>
<script src="{{asset('js/driver/course-update.js')}}"></script>
<script src="{{asset('js/driver/course-delete.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/driver/course-datatable.js') }}"></script>
@endsection