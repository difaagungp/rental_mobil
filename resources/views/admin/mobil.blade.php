@extends('admin.app')
@section('blockhead')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
<link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
@endsection
@section('content')

<header class="page-header">
    <h2>List Mobil</h2>


</header>

<!-- start: page -->
<div class="container">
    <div class="card-body">
        <button class="btn btn-primary" style="margin-bottom: 1rem;" data-toggle="modal" data-target="#modalTambah">Tambah Mobil</button>
        <table class="table table-responsive-md mb-0 mobil">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Mobil</th>
                    <th>Jenis Mobil</th>
                    <th>Merk Mobil</th>
                    <th>Harga Sewa Mobil</th>
                    <th>Jumlah Mobil</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>


<!-- modalcreate -->
<div class="modal fade" id="modalTambah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Mobil</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{route('tambah_mobil')}}" method="POST" enctype="multipart/form-data" class="tambah-mobil">
                    @csrf
                    <form>
                        <div class="form-group">
                            <label class="InputName">Nama Mobil</label>
                            <input type="text" name="nama" placeholder="Nama Mobil" id="nama" class="form-control">
                            @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputJenis">Jenis Mobil</label>
                            <input type="text" class="form-control" id="jenis" name="jenis" placeholder="Jenis Mobil">
                            @error('jenis')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Merk Mobil</label>
                            <input type="text" class="form-control" id="merk" name="merk" placeholder="Merk Mobil">
                            @error('merk')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Harga Sewa Mobil</label>
                            <input type="text" class="form-control" id="harga" name="harga" placeholder="Harga Sewa Mobil">
                            @error('harga')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Jumlah Mobil</label>
                            <input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah Mobil">
                            @error('jumlah')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Bahan Bakar Mobil</label>
                            <input type="text" class="form-control" id=bahabakar name="bahanbakar" placeholder="Bahan Bakar Mobil">
                            @error('bahanbakar')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Gambar Mobil</label>
                            <input type="file" class="form-control" id="gambar" name="gambar">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary btn-tambah" type="submit">Save changes</button>
                        </div>
                    </form>
            </div>

        </div>
    </div>
</div>

<!-- modaledit -->
@foreach ($mobil as $ms)
<div class="modal fade" id="modalEdit{{$ms->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Mobil</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/admin/home/mobil/{{$ms->id}}" method="POST" enctype="multipart/form-data" class="edit-mobil">
                    @csrf
                    @method('PUT')
                    <form>
                        <div class="form-group">
                            <label class="InputName">Nama Mobil</label>
                            <input type="text" name="nama" placeholder="Nama Mobil" id="nama" class="form-control" value="{{$ms->nama_mobil}}">
                            @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputJenis">Jenis Mobil</label>
                            <input type="text" class="form-control" id="jenis" name="jenis" placeholder="Jenis Mobil" value="{{$ms->jenis_mobil}}">
                            @error('jenis')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Merk Mobil</label>
                            <input type="text" class="form-control" id="merk" name="merk" placeholder="Merk Mobil" value="{{$ms->merk_mobil}}">
                            @error('merk')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Harga Sewa Mobil</label>
                            <input type="text" class="form-control" id="harga" name="harga" placeholder="Harga Sewa Mobil" value="{{$ms->harga_mobil}}">
                            @error('harga')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Jumlah Mobil</label>
                            <input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah Mobil" value="{{$ms->armada_mobil}}">
                            @error('jumlah')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Bahan Bakar Mobil</label>
                            <input type="text" class="form-control" id=bahabakar name="bahanbakar" placeholder="Bahan Bakar Mobil" value="{{$ms->bahanbakar_mobil}}">
                            @error('bahanbakar')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Gambar Mobil</label>
                            <input type="file" class="form-control" id="gambar" name="gambar" value="{{$ms->gambar_mobil}}">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary btn-edit" type="submit">Save changes</button>
                        </div>
                    </form>
            </div>

        </div>
    </div>
</div>
@endforeach

<!-- modaldelete -->
@foreach ($mobil as $ms)
<div class="modal fade" id="modalDelete{{$ms->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Delete Driver</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Apakah anda yakin menghapus Mobil ini? : <span>{{$ms->nama_mobil}} </span></h4>
            </div>
            <div class="modal-footer">
                <form action="/admin/home/mobil/{{$ms->id}}" method="POST" class="delete-mobil">
                    @csrf
                    @method('delete')
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-delete-mobil">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection

@section('blockfoot')
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{asset('js/mobil/course-create.js')}}"></script>
<script src="{{asset('js/mobil/course-update.js')}}"></script>
<script src="{{asset('js/mobil/course-delete.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/mobil/course-datatable.js') }}"></script>

@endsection