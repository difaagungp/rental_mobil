@extends('admin.app')
@section('blockhead')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
<link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
@endsection
@section('content')
<header class="page-header">
    <h2>List Peminjaman Mobil</h2>


</header>

<!-- start: page -->
<div class="card-body">
    <table class="table table-responsive-md mb-0 pinjam">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama Penyewa</th>
                <th>Nama Mobil</th>
                <th>Tipe Sewa</th>
                <th>Waktu Sewa</th>
                <th>Tanggal Sewa</th>
                <th>Total Harga</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
@endsection

@section('blockfoot')
<script>
    $('.peminjaman').addClass('nav-active');
    $('.peminjaman').addClass('nav-expanded');
    $('.proses').addClass('nav-active');
</script>
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/transaksi/course-datatable-proses.js') }}"></script>
@endsection