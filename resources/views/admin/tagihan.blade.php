@extends('admin.app')
@section('blockhead')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
<link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
@endsection
@section('content')
<header class="page-header">
    <h2>Tagihan</h2>
</header>

<div class="card-body">
    <table class="table table-responsive-md mb-0 tagihan">
        <thead>
            <tr>
                <th>#</th>
                <th>Id Tagihan</th>
                <th>Nama Penyewa</th>
                <th>Total Tagihan</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

@foreach ($bayar as $br)
<div class="modal fade" id="modalEdit{{$br->id_tagihan}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Bukti Pembayaran</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="" method="POST">
                    @csrf
                    <form>
                        <div class="form-group">
                            <img src="{{asset('tagihan/'.$br->tagihan->gambar_tagihan)}}" alt="Penyewa Belum Upload Bukti" style="width: 30rem;">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
            </div>

        </div>
    </div>
</div>
@endforeach
@endsection

@section('blockfoot')
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/tagihan/course-datatable.js') }}"></script>
@endsection