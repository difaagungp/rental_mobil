@extends('user.app')

@section('content')
<header class="page-header">
    <h2>Detail Sewa</h2>
</header>

<div class="container">
    <div class="card border-0 shadow-0">
        <div class="card-header border-0" style="width: 30rem;">
            <div class="row">
                <a href="/home/pinjaman" class="button" style="margin-left: 5%;"><i class="fas fa-backward"></i></a>
                <div class="card-title" style="margin-left: 4%;">Detail Sewa Mobil</div>
            </div>

        </div>
        <div class="card-body" style="width: 30rem;">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <td class="text-muted w-50">Nama Penyewa</td>
                        <th class="text-right">{{$detail->user->name}}</th>
                    </tr>
                    <tr>
                        <td class="text-muted w-50">Tipe Sewa</td>
                        <th class="text-right">{{$detail->tipe_sewa}}</th>
                    </tr>
                    <tr>
                        <td class="text-muted w-50">Waktu Sewa</td>
                        <th class="text-right">{{$detail->waktu_sewa}} Hari</th>
                    </tr>
                    <tr>
                        <td class="text-muted w-50">Tanggal Sewa</td>
                        <th class="text-right">{{$detail->tanggal_sewa}}</th>
                    </tr>
                    <tr>
                        <td class="text-muted w-50">Harga Sewa</td>
                        <th class="text-right">{{$detail->harga_sewa}}</th>
                    </tr>
                    <tr>
                        <td class="text-muted w-50">Status Sewa</td>
                        <th class="text-right">{{$detail->status_sewa}}</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection