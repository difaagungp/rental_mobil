@extends('user.app')

@section('content')
<header class="page-header">
    <h2>Edit Profile</h2>
</header>

<div class="container">
    <div class="card border-0 shadow-0">
        <div class="card-header border-0" style="width: 40rem;">

            <div class="card-title">Edit Profile</div>
        </div>
        <div class="card-body" style="width: 40rem;">
            <form action="/home/profile/{{$profil->id}}" method="POST" class="profil">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{$profil->name}}">
                    @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="title">Email</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{$profil->email}}">
                    @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="title">NIK KTP</label>
                    <input type="text" class="form-control" id="ktp" name="ktp" value="{{$profil->ktp}}">
                    @error('ktp')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="title">Nomor Telepon</label>
                    <input type="text" class="form-control" id="telp" name="telp" value="{{$profil->nomor_telepon}}">
                    @error('telp')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="title">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name="alamat" value="{{$profil->alamat}}">
                    @error('alamat')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div>
                    <button type="submit" class="btn btn-primary btn-profil">Edit</button>
                    <a href="/home/profile" type="submit" class="btn btn-danger">Cancel</a>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection

@section('blockfoot')
<script src="{{ asset('js/user/course-update-profil.js') }}"></script>
@endsection