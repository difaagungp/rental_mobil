@extends('user.app')

@section('content')
<header class="page-header">
    <h2>Peminjaman Mobil</h2>


</header>

<!-- start: page -->
@foreach ($mobil as $ms)
<div class="col-xl-6">
    <section class="card card-featured-left card-featured-tertiary mb-3">
        <div class="card-body">
            <img src="{{asset('img/'.$ms->gambar_mobil)}}" alt="Gambar Mobil" class="img" style="width: 15rem;">
            <p><strong>Nama Mobil: {{$ms->nama_mobil}}</strong> </p>
            <p><strong>Total Unit: {{$ms->armada_mobil}}</strong> </p>
            <p>Rp. {{$ms->harga_mobil}}</p>
            @if($ms->armada_mobil == 0)
            <ps><strong>SOLD OUT</strong></ps>
            <a href="/home/{{$ms->id}}/pesan" class="btn btn-primary" hidden>Pesan</a>
            @else
            <a href="/home/{{$ms->id}}/pesan" class="btn btn-primary">Pesan</a>
            @endif
        </div>
    </section>
</div>
@endforeach
<!-- end: page -->
@endsection