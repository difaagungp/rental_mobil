@extends('user.app')

@section('content')
<header class="page-header">
    <h2>Detail Sewa Mobil</h2>
</header>


<!-- start: page -->

<div class="container">
    <div class="row">
        <div class="col">
            <div class="card border-0 shadow-0">
                <div class="card-header border-0" style="width: 18rem;">
                    <div class="card-title">Informasi Mobil</div>
                </div>
                <div class="card-body" style="width: 18rem;">
                    <table class="table-borderless">
                        <tbody>
                            <tr>
                                <td class="text-muted w-50">Nama Mobil</td>
                                <th class="text-right">{{$mobil->nama_mobil}}</th>
                            </tr>
                            <tr>
                                <td class="text-muted w-50">Merk Mobil</td>
                                <th class="text-right">{{$mobil->merk_mobil}}</th>
                            </tr>
                            <tr>
                                <td class="text-muted w-50">Jenis Mobil</td>
                                <th class="text-right">{{$mobil->jenis_mobil}}</th>
                            </tr>
                            <tr>
                                <td class="text-muted w-50">Bahan Bakar Mobil</td>
                                <th class="text-right">{{$mobil->bahanbakar_mobil}}</th>
                            </tr>
                            <tr>
                                <td class="text-muted w-50">Harga Sewa Mobil</td>
                                <th class="text-right">Rp.{{$mobil->harga_mobil}}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="card border-0 shadow-0">
                <div class="card-header border-0">
                    <div class="card-title">Detail Sewa</div>
                </div>
                <div class="card-body">
                    <form action="/home" method="POST" class="sewa-mobil">
                        @csrf
                        <input type="text" class="form-control" id="id" name="id" value="{{$nama->id}}" hidden>
                        <input type="text" class="form-control" id="id_mobil" name="id_mobil" value="{{$mobil->id}}" hidden>
                        <div class="form-group">
                            <label for="title">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" value="{{$nama->name}}" readonly>
                            @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="title">Tipe Peminjaman</label>
                            <select class="form-control mb-3" id="tipe" name="tipe">
                                <option value="hanya mobil">Hanya Mobil</option>
                                <option value="mobil dan supir">Mobil dan Supir</option>
                            </select>
                        </div>
                        <div class=" form-group">
                            <label for="title">Lama Sewa</label>
                            <div class="input-group">
                                <input type="number" min="1" class="form-control" id="hari" name="hari" value="{{old('hari')}}">
                                @error('hari')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="input-group-append">
                                    <label class="input-group-text">Hari</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title">Tanggal Sewa</label>
                            <input data-plugin-datepicker="" data-plugin-options='{
                                    "format": "dd-mm-yyyy"
                                }' type="text" class="form-control" id="tanggal" name="tanggal" value="{{old('title')}}" autocomplete="off">
                            @error('tanggal')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="title">Harga</label>
                            <input type="number" class="form-control" id="harga" name="harga" value="{{$mobil->harga_mobil}}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="title">Total</label>
                            <input type="text" class="form-control" id="total" name="total" value="{{old('total')}}" readonly>

                        </div>
                        <button type="submit" class="btn btn-primary btn-sewa" style="margin-top: 1rem;">Pesan Sekarang</button>

                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
@endsection

<!-- start: page -->

<!-- end: page -->
@section('blockfoot')
<script type="text/javascript">
    $(document).ready(function() {
        $("#hari, #harga, #tipe").change(function() {
            if ($("#tipe").val() === "mobil dan supir") {
                var harga = $("#harga").val();
                var hari = $("#hari").val();

                var total = parseInt(hari) * parseInt(harga) + (parseInt(hari) * 100000);
                $("#total").val(total);
                console.log(hari);
                console.log(harga);
            } else {
                var harga = $("#harga").val();
                var hari = $("#hari").val();

                var total = parseInt(hari) * parseInt(harga);
                $("#total").val(total);
                console.log(hari);
                console.log(harga);
            }

        });
    });
</script>
<script src="{{asset('js/transaksi/course-sewa.js')}}"></script>

@endsection