@extends('user.app')
@section('blockhead')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.css') }}" />
@endsection
@section('content')
<header class="page-header">
    <h2>Histori Sewa</h2>
</header>


<div class="card-body">
    <table class="table table-responsive-md mb-0 histori">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama Mobil</th>
                <th>Tipe Sewa</th>
                <th>Waktu Sewa</th>
                <th>Tanggal Sewa</th>
                <th>Total Harga</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <!-- modaledit -->
    @foreach ($peminjaman as $pj)
    <div class="modal fade" id="modalEdit{{$pj->id_tagihan}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Upload Bukti Pembayaran</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/home/pinjaman/{{$pj->id_tagihan}}" method="POST" enctype="multipart/form-data" class="bayar">
                        @csrf
                        @method('PUT')
                        <form>
                            <div class="form-group">
                                <label class="InputName">Total Harga Sewa</label>
                                <input type="text" name="total" placeholder="total Driver" id="total" class="form-control" value="{{$pj->harga_sewa}}" readonly>
                                @error('total')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="InputName">Upload Gambar</label>
                                <input type="file" name="gambar" placeholder="gambar" id="gambar" class="form-control">
                                @error('gambar')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button class="btn btn-primary btn-bayar" type="submit">Bayar Sekarang</button>
                            </div>
                        </form>
                </div>

            </div>
        </div>
    </div>
    @endforeach
    @endsection

    @section('blockfoot')
    <script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
    <script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/user/course-datatable-histori.js') }}"></script>
    <script src="{{ asset('js/user/course-update.js') }}"></script>
    @endsection