@extends('user.app')

@section('content')
<header class="page-header">
    <h2>My Profile</h2>
</header>

<div class="col-md-4 col-sm-6">
    <div class="container">
        <div class="card border-0 shadow-0">
            <div class="card-header border-0" style="width: 30rem;">
                <div class="row">
                    <a href="/home/profile/edit_profile" class="button" style="margin-left: 5%;"><i class="fas fa-edit"></i></a>
                    <div class="card-title" style="margin-left: 4%;">My Profile</div>
                </div>

            </div>
            <div class="card-body" style="width: 30rem;">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <td class="text-muted w-50">Nama</td>
                            <th class="text-right">{{$profil->name}}</th>
                        </tr>
                        <tr>
                            <td class="text-muted w-50">Email</td>
                            <th class="text-right">{{$profil->email}}</th>
                        </tr>
                        <tr>
                            <td class="text-muted w-50">NIK KTP</td>
                            <th class="text-right">{{$profil->ktp}}</th>
                        </tr>
                        <tr>
                            <td class="text-muted w-50">Nomor Telepon</td>
                            <th class="text-right">{{$profil->nomor_telepon}}</th>
                        </tr>
                        <tr>
                            <td class="text-muted w-50">Alamat</td>
                            <th class="text-right">{{$profil->alamat}}</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection