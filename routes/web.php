<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Auth::routes(['verify' => true]);

Route::group(['middleware' => ['is_admin:1']], function () {
    Route::get('/admin/home/customer', 'CustomerController@customer');
    Route::get('/admin/home/customer/list', 'CustomerController@list_customer');
    Route::put('/admin/home/customer/{id}', 'CustomerController@edit_cust');
    Route::delete('/admin/home/customer/{id}', 'CustomerController@delete_cust');
    Route::get('admin/home', 'HomeController@adminHome')->name('admin.home');
    Route::get('/admin/home/mobil', 'MobilController@mobil');
    Route::get('/admin/home/mobil/list', 'MobilController@list_mobil');
    Route::get('/admin/home/armada', 'ArmadaController@armada');
    Route::get('/admin/home/armada/list', 'ArmadaController@list_armada');
    Route::put('/admin/home/mobil/{id}', 'MobilController@edit_mobil');
    Route::delete('/admin/home/mobil/{id}', 'MobilController@delete_mobil');
    Route::post('/admin/home/mobil/tambah', 'MobilController@tambah_mobil')->name('tambah_mobil');
    Route::post('/admin/home/armada/tambah', 'ArmadaController@tambah_armada')->name('tambah_armada');
    Route::put('/admin/home/armada/{id}', 'ArmadaController@edit_armada');
    Route::delete('/admin/home/armada/{id}', 'ArmadaController@delete_armada');
    Route::get('/admin/home/driver', 'DriverController@driver');
    Route::get('/admin/home/driver/list', 'DriverController@list_driver');
    Route::put('/admin/home/driver/{id}', 'DriverController@edit_driver');
    Route::delete('/admin/home/driver/{id}', 'DriverController@delete_driver');
    Route::post('/admin/home/driver/tambah', 'DriverController@tambah_driver')->name('tambah_driver');
    Route::get('/admin/home/peminjaman', 'TransaksiController@list_sewa');
    Route::get('/admin/home/peminjaman/list', 'TransaksiController@sewa_list');
    Route::get('/admin/home/disewa', 'TransaksiController@list_disewa');
    Route::get('/admin/home/disewa/list', 'TransaksiController@disewa_list');
    Route::put('/admin/home/disewa/{id}', 'TransaksiController@update_disewa');
    Route::get('/admin/home/selesai', 'TransaksiController@list_selesai');
    Route::get('/admin/home/selesai/list', 'TransaksiController@selesai_list');
    Route::get('/admin/home/peminjaman/confirm/{id}', 'TransaksiController@confirm_sewa')->name('confirm');
    Route::put('/admin/home/peminjaman/{id}', 'TransaksiController@update_confirm');
    Route::get('/admin/home/tagihan', 'TransaksiController@tagihan');
    Route::get('/admin/home/tagihan/list', 'TransaksiController@tagihan_list');
    Route::post('/mark-as-read/{id}/{id_tagihan}', 'TransaksiController@markNotification')->name('markNotification');
});


Route::group(['middleware' => ['is_admin:0']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/home', 'TransaksiController@sewa');
    Route::get('/home/profile', 'ProfilController@profile');
    Route::get('/home/profile/edit_profile', 'ProfilController@edit_profil');
    Route::put('/home/profile/{id}', 'ProfilController@update_profil');
    Route::get('/home/pinjaman', 'TransaksiController@histori_sewa');
    Route::get('/home/pinjaman/list', 'TransaksiController@histori_sewa_list');
    Route::get('/home/{id}/pesan', 'TransaksiController@pesan');
    Route::put('/home/pinjaman/{id}', 'TransaksiController@update_bayar');
    Route::get('/home/pinjaman/detail/{id}', 'TransaksiController@detail_sewa');
});
